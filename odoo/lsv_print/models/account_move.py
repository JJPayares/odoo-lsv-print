"""
"""
import logging
from odoo import models, fields
from odoo.exceptions import UserError, ValidationError, AccessError

_logger = logging.getLogger(__name__)

class AccountMove(models.Model):
    """
    """
    _name = "account.move"
    _inherit = "account.move"
    
    additional_information = fields.Text(string="Additional information",required=False)
    last_print_date = fields.Datetime(required=False, string="Last Print Date")
    print_ids = fields.One2many('lsv.print.print', 'move_id', string='Prints')
    
    def _create_print_instance(self):
        """
        """
        
        try:
            if not self.env.user.printer_id:
                raise UserError("The user hasn't printer assigned, please assign a printer first.")
            
            self.env['lsv.print.print'].create([{
                'move_id': self.id,
                'printer_id' : self.env.user.printer_id.id,
            }])
        except(AccessError, ValidationError, UserError) as error:
            raise UserError(error)
        
        
    def action_invoice_sent(self):
        """
        """
        
        self._create_print_instance()
        
        output = super(AccountMove, self).action_invoice_sent()
        return output