"""
Python module to generate all reports
"""

import logging
import string
from odoo import models, fields, api, tools
_logger = logging.getLogger(__name__)


class PrintReport(models.Model):
    """
    Model class to generate the general prints report
    """
    
    _name = "lsv.print.prints.report"
    _auto = False
    
    id = fields.Integer(readonly=True, string="ID")
    print_write_date = fields.Datetime(readonly=True, string="Print Date")
    print_state = fields.Char(readonly=True, string="State")
    print_partner_id = fields.Integer(readonly=True, string="User ID")
    print_partner_name = fields.Char(readonly=True, string="User")
    printer_id = fields.Integer(readonly=True, string="Printer id")
    printer_name = fields.Char(readonly=True, string="Printer")
    move_id = fields.Integer(readonly=True, string="Move Id")
    move_name = fields.Char(readonly=True, string="Move")
    move_state = fields.Char(readonly=True, string="Move State")
    move_type = fields.Char(readonly=True, string="Move Type")
    move_amount_total = fields.Float(readonly=True, string="Move total")
    move_invoice_date = fields.Date(readonly=True, string="MOve Invoice Date")
    move_last_print_date = fields.Datetime(readonly=True, string="Move Last print date")
    move_partner_id = fields.Integer(readonly=True, string="Move Partener id")
    move_partner_name = fields.Char(readonly=True, string="MOve Partener")
    
    
    #@api.model_cr
    def init(self):
        """
        Create a lsv print report
        """
        tools.drop_view_if_exists(self._cr,"lsv_print_prints_report")
        self._cr.execute(
            """
            CREATE VIEW lsv_print_prints_report AS (
                SELECT
                    LPP.id AS id
                    , LPP.write_date AS print_write_date
                    , LPP.state AS print_state
                    , LPP.write_uid AS print_partner_id
                    , LPPRP.display_name AS print_partner_name
                    , LPPR.id AS printer_id
                    , CONCAT(LPPRT.name, ' ' , LPPR.name) AS printer_name
                    , AM.id AS move_id
                    , AM.name AS move_name
                    , AM.state AS move_state
                    , AM.move_type AS move_type
                    , AM.amount_total AS move_amount_total
                    , AM.invoice_date AS move_invoice_date
                    , AM.last_print_date AS move_last_print_date
                    , AMRP.id AS move_partner_id
                    , AMRP.display_name AS move_partner_name
                FROM
                    lsv_print_print LPP
                    INNER JOIN res_partner LPPRP ON LPPRP.id = LPP.write_uid
                    INNER JOIN lsv_print_printer LPPR ON LPPR.id = LPP.printer_id
                    INNER JOIN lsv_print_printer_type LPPRT ON LPPRT.id = LPPR.printer_type_id
                    INNER JOIN account_move AM ON AM.id = LPP.move_id
                    INNER JOIN res_partner AMRP ON AMRP.id = AM.partner_id
                ORDER BY
                    LPP.id DESC            
            )
            """
        )

    