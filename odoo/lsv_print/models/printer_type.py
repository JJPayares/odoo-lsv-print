"""
"""

from odoo import models, fields

class PrinterType(models.Model):
    """
    """
    
    _name = 'lsv.print.printer.type'
    _description = 'Manage the printer types used inside LSV Print module'
    _order ='id'
    
    name = fields.Char(string='Name', required=True)
    printer_ids = fields.One2many('lsv.print.printer', inverse_name='printer_type_id')
    