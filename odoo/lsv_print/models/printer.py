"""
This is a pythin module to manage the lsv.print.printer module.
"""

from odoo import models, fields


class Printer(models.Model):
    """
    Lsv.print.printer module whit all attributes and logic
    """
    
    _name = "lsv.print.printer"
    _description = "Printer model used to manage printer information."
    _rec_name = "name"
    _order = "create_date"
    
    name = fields.Char(string='Name', required=True)
    model = fields.Char(string='Model', required=True)
    state = fields.Selection([('avilable', 'Avilable'),('unavailable', 'Unavialable'),('busy','Busy')],string='State', required=True)
    
    print_ids = fields.One2many('lsv.print.print', inverse_name = 'printer_id', string='Prints')
    
    printer_type_id = fields.Many2one( 'lsv.print.printer.type', string='Type', required =True)
    
    user_ids = fields.One2many('res.users', inverse_name='printer_id', string='Users related')