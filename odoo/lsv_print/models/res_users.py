"""
This si a python module to modify the res_user module
"""

from odoo import models, fields


class ResUser(models.Model):
    """
    Res User inherit model
    """
    
    
    _inherit = 'res.users'
    
    account_move_ids = fields.One2many('account.move','user_id',string='Accoutn move id')
    
    printer_id = fields.Many2one('lsv.print.printer', string="Printer", required=False, default=None)
    
    