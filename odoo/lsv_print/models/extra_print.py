"""
"""

from odoo import models, fields

class ExtraPrint(models.Model):
    """
    """
    
    _name = 'lsv.print.extra.print'
    _inherit = 'lsv.print.print'
    
    extra_params = fields.Text(string='Extra Params', required=False)
    
    is_sent = fields.Boolean(default=False, string='Is Sent')