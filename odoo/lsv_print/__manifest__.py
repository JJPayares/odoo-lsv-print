{
    "name": 'LSV Print Module',
    "author": 'LSV-Tech',
    "version":'1.0.0-dev23',
    "summary": 'This module manage prints and printers',
    "depends":['sale_management', 'account', 'contacts'],    
    "data" : [
        'security/user.groups.xml',
        'security/ir.model.access.csv',
        'data/printer_types_data.xml',
        'views/menuitems.xml',
        'views/printer_type_views.xml',
        'views/printer_views.xml',
        'views/res_users_views.xml',
        'views/print_views.xml',
        'views/extra_prints_views.xml',
        'views/account_move_views.xml',
        'views/prints_report_views.xml',]
}